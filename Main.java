import java.util.Arrays;

public class Main {
    /**
     * Cho 3 mang, tim cac phan tu xuat hien o ca 3 mang da cho
     */
    public static void main(String[] args) {
        int[] arrOne = {1,4,2,6,7,4};
        int[] arrTwo = {-1, 0, 6, 4};
        int[] arrThree = {4, 2, 0, 7};
        findElementCommon(arrOne, arrTwo, arrThree);
    }

    static void findElementCommon(int[] arrOne, int[] arrTwo, int[] arrThree) {
        // Sort 3 array
        Arrays.sort(arrOne);
        Arrays.sort(arrTwo);
        Arrays.sort(arrThree);
        int i = 0, j = 0, k = 0;
        while (i < arrOne.length && j < arrTwo.length && k < arrThree.length) {
            if (arrOne[i] == arrTwo[j] && arrTwo[j] == arrThree[k]) {
                System.out.println(arrOne[i]);
                i++;
                j++;
                k++;
            } else if (arrOne[i] < arrTwo[j]) {
                i++;
            } else if (arrTwo[j] < arrThree[k]) {
                j++;
            } else {
                k++;
            }
        }
    }
}
